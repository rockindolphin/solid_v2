import { events } from "../events.js";
import IconClose from '../components/icons/icon_files/IconClose.vue';
import ElCustomScroll from '../components/ElCustomScroll.vue';

export default {
    components: {
        IconClose,
        ElCustomScroll
    },
    props: {
        visible: {
            type: Boolean,
            default: false
        },
        name: {
            type: String,
            default: 'default-popup-name'
        }
    },
    data() {
        return {
            isVisible: this.visible
        }
    },
    computed: {
        hasFooter() {
            return !!this.$slots.footer || !!this.$scopedSlots.footer;
        }
    },
    methods: {
        closePopup() {
            this.isVisible = false;
            this.$evbus.$emit(events.EV_APP_TOGGLE_PAGE_SCROLL, true);
            this.$emit('popup-close', true);
        },
        showPopup() {
            this.isVisible = true;
            this.$evbus.$emit(events.EV_APP_TOGGLE_PAGE_SCROLL, false);
            this.$emit('popup-open', true);
        },
        onPopupShow() {
            this.showPopup();
        },
        onPopupClose() {
            this.closePopup();
        }
    },
    mounted() {
        this.$evbus.$on(events.EV_APP_OPEN_POPUP, (data) => {
            if (data.name === this.name) {
                this.onPopupShow(data);
            }
        });
        this.$evbus.$on(events.EV_APP_CLOSE_POPUP, (data) => {
            if (data.name === this.name) {
                this.onPopupClose(data);
            }
        });
        this.$evbus.$on(events.EV_APP_CLOSE_ALL_WINDOWS, () => {
            if (this.isVisible) {
                this.onPopupClose();
            }
        });
    }
}