export default {
    components: {

    },
    props: {
        initialBreakpoints: {
            type: Object,
            required: true
        }
    },
    data() {
        return {
            breakpoints: this.initialBreakpoints,
            currentBreakpointKey: ''
        }
    },
    computed: {

    },
    methods: {
        setBreakpoinKey(){
            if (window.innerWidth >= 0 && window.innerWidth < this.breakpoints.sm) {
                this.currentBreakpointKey = 'xs';
            } else if (window.innerWidth >= this.breakpoints.sm && window.innerWidth < this.breakpoints.md) {
                this.currentBreakpointKey = 'sm';
            } else if (window.innerWidth >= this.breakpoints.md && window.innerWidth < this.breakpoints.lg) {
                this.currentBreakpointKey = 'md';
            } else if (window.innerWidth >= this.breakpoints.lg && window.innerWidth < this.breakpoints.xl) {
                this.currentBreakpointKey = 'lg';
            } else if (window.innerWidth >= this.breakpoints.xl) {
                this.currentBreakpointKey = 'xl';
            }
        }
    },
    mounted() {
        Object.keys(this.breakpoints).map( key => {
            window.matchMedia(`(min-width:${this.breakpoints[key]}px)`).addListener(this.setBreakpoinKey);
        });
        this.setBreakpoinKey();
    },
    beforeDestroy(){
        Object.keys(this.breakpoints).map(key => {
            window.matchMedia(`(min-width:${this.breakpoints[key]}px)`).removeListener(this.setBreakpoinKey);
        });
    }
}