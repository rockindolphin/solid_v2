let anketData = {
    questions: [
        {
            title: 'Укажите ваш возраст',
            variants: [
                {
                    value: 'до 30 лет',
                    points: 3
                },
                {
                    value: '30 - 50 лет',
                    points: 2
                },
                {
                    value: '50 - 60 лет',
                    points: 1
                },
                {
                    value: 'более 60 лет',
                    points: 0
                }
            ],
            selected: null,
            section: 1
        },
        {
            title: 'Инвестировали ли Вы ранее в инвестиционные инструменты?',
            variants: [
                {
                    value: 'Не инвестировал ранее',
                    points: 3
                },
                {
                    value: 'Есть опыт инвестирования в паевые фонды и доверительное управление',
                    points: 2
                },
                {
                    value: 'Инвестировал через брокерский счёт',
                    points: 1
                },
                {
                    value: 'Активно инвестировал через брокерский счет, используя рисковые инструменты (Forex, срочный рынок)',
                    points: 0
                }
            ],
            selected: null,
            section: 1
        },
        {
            title: 'На какой срок готовы инвестировать?',
            variants: [
                {
                    value: 'Менее 1 года',
                    points: 0
                },
                {
                    value: 'От 1 года до 3 лет',
                    points: 1
                },
                {
                    value: 'От 3 до 5 лет',
                    points: 2
                },
                {
                    value: 'Более 5 лет',
                    points: 3
                }
            ],
            selected: null,
            section: 1
        },
        {
            title: 'Какой объем денежных средств Вы готовы инвестировать?',
            variants: [
                {
                    value: 'До 300 тыс. рублей',
                    points: 0
                },
                {
                    value: 'От 300 тыс. до 1 млн. рублей',
                    points: 1
                },
                {
                    value: 'От 1 до 5 млн. рублей',
                    points: 2
                },
                {
                    value: 'От 5 до 15 млн. рублей',
                    points: 3
                },
                {
                    value: 'Более 15 млн. рублей',
                    points: 4
                }
            ],
            selected: null,
            section: 1
        },
        {
            title: 'Какой вариант инвестирования относительно доходности банковского депозита (в рублях) Вы бы выбрали?',
            variants: [
                {
                    value: 'Доходность банковского депозита, при минимальных рисках',
                    points: 0
                },
                {
                    value: 'Доходность банковского депозита + 3-6% годовых',
                    points: 4
                },
                {
                    value: 'Доходность банковского депозита + 6-10% годовых',
                    points: 9
                },
                {
                    value: 'Доходность банковского депозита + 10-14% годовых с существенным уровнем риска',
                    points: 14
                },
                {
                    value: 'Доходность банковского депозита + 14-18% годовых, при высоких рисках',
                    points: 20
                },
                {
                    value: 'Инвестировать в самые рисковые инструменты с доходностью более чем 18% годовых',
                    points: 25
                }
            ],
            selected: null,
            section: 2
        },
        {
            title: 'Допустимый уровень изменения стоимости ваших активов?',
            variants: [
                {
                    value: 'Не допускаю снижения стоимости активов',
                    points: 0
                },
                {
                    value: 'Допускаю снижение стоимости активов до 5%',
                    points: 3
                },
                {
                    value: 'Допускаю снижение стоимости активов до 10%',
                    points: 6.01
                },
                {
                    value: 'Допускаю снижение стоимости активов до 15%',
                    points: 6.001
                },
                {
                    value: 'Допускаю снижение стоимости активов до 25%',
                    points: 12
                },
                {
                    value: 'Готов к существенному понижению стоимости активов с расчетом на последующий рост',
                    points: 16
                }
            ],
            selected: null,
            section: 2
        }
    ],
    answersTable: {
        '0': {
            '0-8': 'konservativnyy',
            '9-10': 'umerenno-konservativnyy',
            '11-19': 'umerenno-konservativnyy',
            '20-35': 'ratsionalnyy',
            '36-37': 'ratsionalnyy',
            '38-41': 'umerenno-agressivnyy'
        },
        '1-2': {
            '0-8': 'konservativnyy',
            '9-10': 'umerenno-konservativnyy',
            '11-19': 'umerenno-konservativnyy',
            '20-35': 'ratsionalnyy',
            '36-37': 'umerenno-agressivnyy',
            '38-41': 'umerenno-agressivnyy'
        },
        '3-5': {
            '0-8': 'konservativnyy',
            '9-10': 'umerenno-konservativnyy',
            '11-19': 'ratsionalnyy',
            '20-35': 'umerenno-agressivnyy',
            '36-37': 'umerenno-agressivnyy',
            '38-41': 'aggressive'
        },
        '6-8': {
            '0-8': 'umerenno-konservativnyy',
            '9-10': 'ratsionalnyy',
            '11-19': 'ratsionalnyy',
            '20-35': 'umerenno-agressivnyy',
            '36-37': 'agressivnyy',
            '38-41': 'agressivnyy'
        },
        '9-11': {
            '0-8': 'umerenno-konservativnyy',
            '9-10': 'ratsionalnyy',
            '11-19': 'umerenno-agressivnyy',
            '20-35': 'agressivnyy',
            '36-37': 'agressivnyy',
            '38-41': 'spekulyativnyy'
        },
        '12-14': {
            '0-8': 'umerenno-konservativnyy',
            '9-10': 'ratsionalnyy',
            '11-19': 'umerenno-agressivnyy',
            '20-35': 'agressivnyy',
            '36-37': 'spekulyativnyy',
            '38-41': 'spekulyativnyy'
        }
    },
    results: {
        'konservativnyy': {
            title: 'Консервативный',
            description: 'Ваша цель - сохранение и защита капитала. Вы готовы размещать средства только в безрисковые консервативные инструменты. Доходы могут быть получены на уровне существующих процентных ставок по депозитам, которые могут не соответствовать темпам инфляции.',
            suggestions: [
                { name: 'Инвестиции с защитой капитала', url: '/investment_management/investments_with_guarantee/' },
                { name: 'Инвестиционно-банковские услуги', url: '/capital_markets/assets_securitization/' },
                { name: 'Индивидуальный инвестиционный счет', url: '/investment_management/iia/' }
            ]
        },
        'umerenno-konservativnyy': {
            title: 'Умеренно-консервативный',
            description: 'Вы готовы принять минимальный уровень инвестиционного риска, выраженный в возможности незначительной потери капитала,в обмен на потенциальную возможность получить более высокую доходность на уровне банковского депозита + 3-6% годовых в рубляхи/или защитить свои средства от инфляции.',
            suggestions: [
                { name: 'Фонд облигаций &laquo;ФДИ Солид&raquo;', url: '/investment_management/pif/' },
                { name: 'Инвестиции с защитой капитала', url: '/investment_management/investments_with_guarantee/' },
                { name: 'Индивидуальный инвестиционный счет', url: '/investment_management/iia/' }
            ]
        },
        'ratsionalnyy': {
            title: 'Рациональный',
            description: 'Вы готовы принять разумный уровень инвестиционного риска в обмен на потенциальную возможность получить доход на уровне банковского депозита + 6-10% годовых в рублях. В этом случае стоимость капитала может колебаться, а также упасть ниже суммы Ваших первоначальных инвестиций в краткосрочной перспективе.',
            suggestions: [
                { name: 'Инвестиционно-банковские услуги', url: '/capital_markets/assets_securitization/' },
                { name: 'Персональный брокер', url: '/investment_management/personal_consulting/' },
                { name: 'Индивидуальный инвестиционный счет', url: '/investment_management/iia/' }
            ]
        },
        'umerenno-agressivnyy': {
            title: 'Умеренно-агрессивный',
            description: 'Вы готовы принять высокий уровень инвестиционного риска и колебаний стоимости в кратко- и среднесрочной перспективе в обмен на потенциальную возможность получить доход на уровне банковского депозита + 10-14% годовых в рублях. Стоимость капитала может колебаться, а также упасть ниже суммы Ваших первоначальных инвестиций в течение некоторого периода времени.',
            suggestions: [
                { name: 'Индивидуальный инвестиционный счет', url: '/investment_management/iia/' },
                { name: 'Стратегия &laquo;Дивидендные акции&raquo;', url: '/investment_management/trust_management/keep-and-multiply/dividendnye_aktsii/' },
                { name: 'Секьюритизация активов', url: '/capital_markets/assets_securitization/' }
            ]
        },
        'agressivnyy': {
            title: 'Агрессивный',
            description: 'Вы готовы принять высокий уровень инвестиционного риска и колебаний стоимости в кратко- и среднесрочной перспективе в обмен на потенциальную возможность получить доход на уровне банковского депозита +14-18% годовых в рублях. Стоимость капитала может колебаться, а также упасть значительно ниже суммы Ваших первоначальных инвестиций в течении некоторого периода времени.',
            suggestions: [
                { name: 'Интернет-трейдинг', url: '/trading/internet-trading/' },
                { name: 'Фонд акций &laquo;Солид Глобус&raquo;', url: '/investment_management/pif/' },
                { name: 'Стратегия &laquo;Российские акции 1, 2, 3&raquo;', url: '/investment_management/trust_management/earn/smart-investitsii/' }
            ]
        },
        'spekulyativnyy': {
            title: 'Спекулятивный',
            description: 'Вы готовы принять контролируемый Вами уровень инвестиционного риска и колебаний стоимости в обмен на возможность получить установленный Вами уровень доходности. Вероятно, Вы обладаете самостоятельными навыками принятия грамотных решений об управлении активами и совершении сделок. Вы можете самостоятельно определять и контролировать уровень инвестиционного риска и вероятной доходности, но в случае ряда неудачных решений (сделок), возможно, с использованием «плеча» и производных финансовых инструментов.',
            suggestions: [
                { name: 'Маржинальная торговля', url: '/trading/marginal_trading/' },
                { name: 'Фонд акций &laquo;Солид Индекс ММВБ&raquo;', url: '/investment_management/pif/' },
                { name: 'Стратегия &laquo;Smart Инвестиции&raquo;', url: '/investment_management/trust_management/' }
            ]
        }
    }
};

function getAnketSectionScore(questions, sectionNum){
    return parseInt(questions.reduce((accum, current) => {
        if (current.section === sectionNum) {
            return accum + current.selected || 0;
        } else {
            return accum;
        }
    }, 0));
}

function getRangeMinMax(range){
    let parts = range.split('-');
    return {
        min: parts[0] ? parseInt(parts[0]) : 0,
        max: parts[1] ? parseInt(parts[1]) : 0
    }
}

export { anketData, getAnketSectionScore, getRangeMinMax }