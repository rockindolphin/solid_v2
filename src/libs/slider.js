import Swiper, { Navigation, Pagination, Autoplay, EffectCoverflow } from 'swiper';

Swiper.use([Navigation, Pagination, Autoplay, EffectCoverflow]);

function initSlider( selector, config = {} ){
    let sliders = [...document.querySelectorAll(selector)],
        resp = [];

    sliders.map( slider => {
        let container = slider.querySelector('.swiper-container'),
            pagination = slider.querySelector('.swiper-pagination'),
            prevBtn = slider.querySelector('.slider__btn--prev'),
            nextBtn = slider.querySelector('.slider__btn--next');

        let defaultConfig = {
            speed: 800,
            spaceBetween: 0,
            slidesPerView: 1,
            loop: false,
            pagination: {
                el: pagination,
                type: 'bullets',
                clickable: true
            },
            navigation: {
                nextEl: nextBtn,
                prevEl: prevBtn
            }
        }
        resp.push( new Swiper(container, { ...defaultConfig, ...config }) );
    });

    return resp;
}

export default initSlider;