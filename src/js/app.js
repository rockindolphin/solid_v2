import Vue from 'vue';
import VueScrollmagic from 'vue-scrollmagic';
import YmapPlugin from 'vue-yandex-maps';
import solidComponents from './components/components.js';
import { events } from '../events.js';
import Swiper, { Navigation, Pagination, Autoplay, EffectCoverflow } from 'swiper';
import pagesMethods from './components/pagesMethods.js';

// for ie11
import 'core-js/es/number';
import { customEventPolyfill } from '../libs/customEventPolyfill.js';

customEventPolyfill();
Swiper.use([Navigation, Pagination, Autoplay, EffectCoverflow]);

//set path for webpack chunks (чанки должны лежать рядом с app.js)
var src = document.querySelector('[src*="app.js"]').getAttribute("src");
__webpack_public_path__ = src.substr(0, src.lastIndexOf("/") + 1);

history.scrollRestoration = "manual";

document.addEventListener("DOMContentLoaded", function (event) {

    let breakpoints = JSON.parse(process.env.VUE_APP_BREAKPOINTS);

    Vue.config.productionTip = false

    Vue.prototype.$evbus = new Vue(); // Global event bus

    Vue.use(YmapPlugin);

    Vue.use(VueScrollmagic);

    Vue.config.ignoredElements = ['noindex'];

    Vue.directive('backgroundImage', {
        inserted: function (el) {

            function getImageSrc(img) {
                let scrProp = window.HTMLPictureElement ? 'currentSrc' : 'src';
                return new Promise((resolve, reject) => {
                    if (img.complete) {
                        resolve(img[scrProp]);
                    } else {
                        img.addEventListener('load', function () {
                            resolve(img[scrProp]);
                        }, { once: true });
                        img.addEventListener('error', function () {
                            reject('fallback_img_url');
                        }, { once: true });
                    }
                });
            }

            if (el.nodeName.toUpperCase() === 'PICTURE') {
                getImageSrc(el.querySelector('img')).then(src => {
                    el.parentNode.style.backgroundImage = `url(${src})`;
                });

                if (window.HTMLPictureElement) {
                    Object.keys(breakpoints).map(breakpoint_key => {
                        window.matchMedia(`(min-width:${breakpoints[breakpoint_key]}px)`).addListener(() => {
                            getImageSrc(el.querySelector('img')).then(src => {
                                el.parentNode.style.backgroundImage = `url(${src})`;
                            });
                        });
                    });
                }
            } else {
                getImageSrc(el).then(src => {
                    el.parentNode.style.backgroundImage = `url(${src})`;
                });
            }
            el.style.display = 'none';

        },
    });

    Vue.directive('rangeProgress', {
        inserted: function (el) {
            let min = el.min || 0,
                max = el.max || 100;
            el.style.setProperty('--range-progress', `${(el.value * 100 / (max - min) - min * 100 / max).toFixed(0)}%`);
            el.addEventListener('input', () => {
                el.style.setProperty('--range-progress', `${(el.value * 100 / (max - min) - min * 100 / max).toFixed(0)}%`);
            }, false);
        },
    });

    Vue.filter('money', function (value, symbol = 'руб.') {
        return `${new Intl.NumberFormat('ru-RU', {
            minimumFractionDigits: 0,
            maximumFractionDigits: 2
        }).format(value)} ${symbol}`;
    });

    let app = new Vue({
        el: '.page__body',
        components: solidComponents,
        mixins: [
            pagesMethods
        ],
        data: {
            menuOpen: false,
            isEdge: /Edge/.test(navigator.userAgent),
            isIE: /Trident/.test(navigator.userAgent),
            isSafari: navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1,
            breakpoints: breakpoints,
            currentTab: ''
        },
        computed: {
            animEnabled() {
                return !(this.isEdge || this.isIE);
            }
        },
        filters: {

        },
        methods: {
            toggleMenu() {
                this.menuOpen = !this.menuOpen;
                this.$evbus.$emit(events.EV_APP_TOGGLE_PAGE_SCROLL, !this.menuOpen);
            },
            closeMenu() {
                this.menuOpen = false;
                this.$evbus.$emit(events.EV_APP_TOGGLE_PAGE_SCROLL, !this.menuOpen);
            },
            toggleSubMenu(evt) {
                evt.target.closest('.menu__item').classList.toggle('menu__item--open');
            },
            lockPageScroll: function () {
                if (this.isSafari) {
                    this.scrollPos = {
                        x: window.scrollX,
                        y: window.scrollY
                    }
                }
                document.querySelector('.page').classList.add('page--noscroll');
            },
            unlockPageScroll: function () {
                document.querySelector('.page').classList.remove('page--noscroll');
                if (this.isSafari) {
                    window.scrollTo(this.scrollPos.x, this.scrollPos.y);
                }
            },
            toggleTableRow(evt) {
                evt.target.closest('.table__row').classList.toggle('table__row--open');
            },
            initCssVariables() {
                //ширина скроллбара
                let scrollMeasure = document.createElement('div');
                scrollMeasure.classList.add('scroll__measure');
                document.body.appendChild(scrollMeasure);
                var scrollbarWidth = scrollMeasure.offsetWidth - scrollMeasure.clientWidth;
                document.querySelector(':root').style.setProperty('--scrollbar-width', `${scrollbarWidth}px`);

                //реальный vh - https://css-tricks.com/the-trick-to-viewport-units-on-mobile/
                let vh = window.innerHeight * 0.01;
                document.documentElement.style.setProperty('--vh', `${vh}px`);
                window.addEventListener('resize', () => {
                    let vh = window.innerHeight * 0.01;
                    document.documentElement.style.setProperty('--vh', `${vh}px`);
                });
            },
            registerCssProperties(){
                if (!this.isEdge && !this.isIE) {
                    // custom property for animating gradients rotation
                    if (CSS.registerProperty) {
                        document.body.classList.add('page--hodini-cp-api');
                        CSS.registerProperty({
                            name: '--ang',
                            syntax: '<angle>',
                            initialValue: '0deg',
                            inherits: true
                        });
                    }
                }
            },
            registerEvents(){
                this.$evbus.$on(events.EV_APP_CLOSE_ALL_WINDOWS, () => {
                    if (this.menuOpen) {
                        this.closeMenu();
                    }
                });
                this.$evbus.$on(events.EV_APP_TOGGLE_PAGE_SCROLL, (data) => {
                    data ? this.unlockPageScroll() : this.lockPageScroll();
                });
                this.$evbus.$on(events.EV_APP_TOGGLE_TAB, (data) => {
                    this.currentTab = data.tab;
                });
            },
            initSlider(selector, config = {}){
                let sliders =[...document.querySelectorAll(selector)],
                resp =[];

                sliders.map(slider => {
                    let container = slider.querySelector('.swiper-container'),
                        pagination = slider.querySelector('.swiper-pagination'),
                        prevBtn = slider.querySelector('.slider__btn--prev'),
                        nextBtn = slider.querySelector('.slider__btn--next');

                    let defaultConfig = {
                        speed: 800,
                        spaceBetween: 0,
                        slidesPerView: 1,
                        loop: false,
                        pagination: {
                            el: pagination,
                            type: 'bullets',
                            clickable: true
                        },
                        navigation: {
                            nextEl: nextBtn,
                            prevEl: prevBtn
                        }
                    }
                    resp.push( new Swiper(container, { ...defaultConfig, ...config }) );
                });

                return resp;
            },
            initPageScripts(){
                let pageElem = document.querySelector('.page');

                if (pageElem.classList.contains('page--index')) {
                    this.initFrontPage();
                }

                if (pageElem.classList.contains('page--iis')) {
                    this.initIisPage();
                }

                if (pageElem.classList.contains('page--idu')) {
                    this.initIduPage();
                }

                if (pageElem.classList.contains('page--personal_broker')) {
                    this.initPersonalBrokerPage();
                }

                if (pageElem.classList.contains('page--application')) {
                    this.initApplicationPage();
                }

                if (pageElem.classList.contains('page--docs')) {
                    this.currentTab = 'tab1';
                }

                if (pageElem.classList.contains('page--contacts')) {
                    this.currentTab = 'peterburg';
                    this.$evbus.$on(events.EV_APP_TOGGLE_TAB, (data) => {
                        let bg = document.querySelector('.section--header > .bg__pic');
                        bg.style = `background-image: url(assets/img/cities/${data.tab}.jpg)`;
                    });
                }

                if (pageElem.classList.contains('page--tariffs')) {
                    this.initApplicationPage();
                    this.currentTab = 'tab2';
                }

                if (pageElem.classList.contains('page--evaluation')) {
                    this.initEvaluationPage();
                }

                if (pageElem.classList.contains('page--markets')) {
                    this.initMarketsPage();
                }

                if (pageElem.classList.contains('page--auction')) {
                    this.initApplicationPage();
                }
            },
            openPopup(name, data) {
                this.$evbus.$emit(events.EV_APP_OPEN_POPUP, {
                    name: `popup-${name}`,
                    ...data
                });
            }
        },
        mounted() {
            this.initCssVariables();
            this.registerEvents();
            this.registerCssProperties();
            this.initPageScripts();
        }
    });

    //window.solidApp = app;

})


