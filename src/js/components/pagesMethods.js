export default {
    methods: {
        changeBgPosition(scrollProgress) {
            let figures = [...document.querySelectorAll('.list--parallax li')],
                speedY = [-2, 3, -2, 3],
                speedX = [-1.5, -2, 1, -0.5];
            window.requestAnimationFrame(() => {
                figures.map((figure, index) => {
                    figure.style.transform = `translate3d(${scrollProgress * speedX[index]}px, ${scrollProgress * speedY[index]}px, 0)`;
                })
            })
        },
        initBgParallax() {
            if (!document.querySelector('.page__parallax')) { return; }
            this.parallaxScene = this.$scrollmagic.scene({
                triggerElement: '.page',
                duration: document.documentElement.scrollHeight - window.innerHeight,
                offset: 0,
                triggerHook: 0
            })
                .on('progress', (evt) => {
                    this.changeBgPosition((evt.progress * 100).toFixed(0));
                })
            //.addIndicators({indent: 100});
            this.$scrollmagic.addScene(this.parallaxScene);
        },
        initFrontPage(){
            //parallax
            this.initBgParallax();
        },
        initIisPage() {

            //slider--feedback
            this.initSlider('.slider--feedback', {
                loop: true,
                spaceBetween: 110,
                effect: 'coverflow',
                coverflowEffect: {
                    rotate: 30,
                    slideShadows: false
                }
            })[0];

            //slider--advantages
            function initSliderAdvantages(_this, toggleBreakpoint) {
                if (!toggleBreakpoint.matches) {
                    let breakpoints = {};
                    breakpoints[_this.breakpoints['md']] = {
                        slidesPerView: 3
                    }
                    _this.sliderAdvantages = _this.initSlider('.slider--advantages', {
                        breakpoints: breakpoints,
                        loop: true
                    })[0];
                } else {
                    if (_this.sliderAdvantages) {
                        _this.sliderAdvantages.destroy(true, true);
                    }
                }
            }

            let sliderAdvantagesToggleBreakpoint = window.matchMedia(`(min-width:${this.breakpoints['lg']}px)`);
            sliderAdvantagesToggleBreakpoint.addListener(() => {
                initSliderAdvantages(this, sliderAdvantagesToggleBreakpoint);
            });
            initSliderAdvantages(this, sliderAdvantagesToggleBreakpoint);
        },
        initIduPage() {

            //slider--advantages
            function initSliderAdvantages(_this, toggleBreakpoint) {
                if (!toggleBreakpoint.matches) {
                    let breakpoints = {};
                    breakpoints[_this.breakpoints['md']] = {
                        slidesPerView: 3
                    }
                    _this.sliderAdvantages = _this.initSlider('.slider--advantages', {
                        breakpoints: breakpoints,
                        loop: true
                    })[0];
                } else {
                    if (_this.sliderAdvantages) {
                        _this.sliderAdvantages.destroy(true, true);
                    }
                }
            }

            let sliderAdvantagesToggleBreakpoint = window.matchMedia(`(min-width:${this.breakpoints['lg']}px)`);
            sliderAdvantagesToggleBreakpoint.addListener(() => {
                initSliderAdvantages(this, sliderAdvantagesToggleBreakpoint);
            });
            initSliderAdvantages(this, sliderAdvantagesToggleBreakpoint);
        },
        initPersonalBrokerPage() {

            //slider--advantages
            function initSliderAdvantages(_this, toggleBreakpoint) {
                if (!toggleBreakpoint.matches) {
                    let breakpoints = {};
                    breakpoints[_this.breakpoints['md']] = {
                        slidesPerView: 3
                    }
                    _this.sliderAdvantages = _this.initSlider('.slider--advantages', {
                        breakpoints: breakpoints,
                        loop: true
                    })[0];
                } else {
                    if (_this.sliderAdvantages) {
                        _this.sliderAdvantages.destroy(true, true);
                    }
                }
            }

            let sliderAdvantagesToggleBreakpoint = window.matchMedia(`(min-width:${this.breakpoints['lg']}px)`);
            sliderAdvantagesToggleBreakpoint.addListener(() => {
                initSliderAdvantages(this, sliderAdvantagesToggleBreakpoint);
            });
            initSliderAdvantages(this, sliderAdvantagesToggleBreakpoint);

        },
        initApplicationPage() {

            //slider--application
            function initSliderApplication(_this, toggleBreakpoint) {
                if (!toggleBreakpoint.matches) {
                    let breakpoints = {};
                    breakpoints[_this.breakpoints['md']] = {
                        slidesPerView: 2
                    }
                    _this.sliderApplication = _this.initSlider('.slider--application', {
                        breakpoints: breakpoints,
                        loop: true
                    });
                } else {
                    if (_this.sliderApplication) {
                        _this.sliderApplication.map(slider => {
                            slider.destroy(true, true);
                        });
                    }
                }
            }

            let sliderApplicationToggleBreakpoint = window.matchMedia(`(min-width:${this.breakpoints['lg']}px)`);
            sliderApplicationToggleBreakpoint.addListener(() => {
                initSliderApplication(this, sliderApplicationToggleBreakpoint);
            });
            initSliderApplication(this, sliderApplicationToggleBreakpoint);
        },
        initMarketsPage() {
            this.initApplicationPage();
        },
        initEvaluationPage(){
            //slider--application
            this.initApplicationPage();
        }
    }
}