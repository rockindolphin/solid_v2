import IconLogo from '../../components/icons/icon_files/IconLogo.vue';
import IconLogoTextTop from '../../components/icons/icon_files/IconLogoTextTop.vue';
import IconLogoTextBottom from '../../components/icons/icon_files/IconLogoTextBottom.vue';
import IconMenuToggle from '../../components/icons/icon_files/IconMenuToggle.vue';
import IconCall from '../../components/icons/icon_files/IconCall.vue';
import IconChewronDown from '../../components/icons/icon_files/IconChewronDown.vue';
import IconMapPin from '../../components/icons/icon_files/IconMapPin.vue';
import IconLogIn from '../../components/icons/icon_files/IconLogIn.vue';
import IconSearch from '../../components/icons/icon_files/IconSearch.vue';
import IconAppStore from '../../components/icons/icon_files/IconAppStore.vue';
import IconGooglePlay from '../../components/icons/icon_files/IconGooglePlay.vue';
import IconFilePdf from '../../components/icons/icon_files/IconFilePdf.vue';
import IconChewronLeft from '../../components/icons/icon_files/IconChewronLeft.vue';
import IconStrat from '../../components/icons/icon_files/IconStrat.vue';
import IconMoney from '../../components/icons/icon_files/IconMoney.vue';
import IconCalendar from '../../components/icons/icon_files/IconCalendar.vue';
import IconIdeas from '../../components/icons/icon_files/IconIdeas.vue';
import IconQuality from '../../components/icons/icon_files/IconQuality.vue';
import IconTelegramMen from '../../components/icons/icon_files/IconTelegramMen.vue';
import IconSolidApp from '../../components/icons/icon_files/IconSolidApp.vue';
import IconGuarantee from '../../components/icons/icon_files/IconGuarantee.vue';
import IconRefill from '../../components/icons/icon_files/IconRefill.vue';
import IconIncome from '../../components/icons/icon_files/IconIncome.vue';
import IconMulticurrency from '../../components/icons/icon_files/IconMulticurrency.vue';
import IconFlexible from '../../components/icons/icon_files/IconFlexible.vue';
import IconDocs from '../../components/icons/icon_files/IconDocs.vue';
import IconClose from '../../components/icons/icon_files/IconClose.vue';
import IconDownload from '../../components/icons/icon_files/IconDownload.vue';

import ElDropdown from '../../components/ElDropdown.vue';
import ElPopupSearch from '../../components/ElPopupSearch.vue';
import ElCustomScroll from '../../components/ElCustomScroll.vue';
import DropdownCities from '../../components/api_blocks/DropdownCities.vue';
import SiteMenu from '../../components/api_blocks/SiteMenu.vue';
import SiteSocials from '../../components/api_blocks/SiteSocials.vue';

export default {
    //async imports
    Multiselect: () => import(/* webpackChunkName: `multiselect` */ 'vue-multiselect'),
    ElPopupRiskProfile: () => import(/* webpackChunkName: `popup_risk_profile` */ '../../components/ElPopupRiskProfile.vue'),
    ElPopupAskQuestion: () => import(/* webpackChunkName: `popup_ask_question` */ '../../components/ElPopupAskQuestion.vue'),
    ElPopupBackcall: () => import(/* webpackChunkName: `popup_backcall` */ '../../components/ElPopupBackcall.vue'),
    ElSectionIis: () => import(/* webpackChunkName: `section_iis` */ '../../components/ElSectionIis.vue'),
    ElSectionCalc: () => import(/* webpackChunkName: `section_calc` */ '../../components/ElSectionCalc.vue'),
    ElSectionCalcIdu: () => import(/* webpackChunkName: `section_calc_idu` */ '../../components/ElSectionCalcIdu.vue'),
    ElSectionGraph: () => import(/* webpackChunkName: `section_graph` */ '../../components/ElSectionGraph.vue'),
    ElPopupGallery: () => import(/* webpackChunkName: `el_popup_gallery` */ '../../components/ElPopupGallery.vue'),
    ElListTabs: () => import(/* webpackChunkName: `elList_tabs` */ '../../components/ElListTabs.vue'),
    ElFigureTariff: () => import(/* webpackChunkName: `el_figure_tariff` */ '../../components/ElFigureTariff.vue'),

    FrontBanners: () => import(/* webpackChunkName: `front_banners` */ '../../components/api_blocks/FrontBanners.vue'),
    FrontStrategies: () => import(/* webpackChunkName: `front_strategies` */ '../../components/api_blocks/FrontStrategies.vue'),
    FrontAnalitics: () => import(/* webpackChunkName: `front_analitics` */ '../../components/api_blocks/FrontAnalitics.vue'),
    FrontPress: () => import(/* webpackChunkName: `front_press` */ '../../components/api_blocks/FrontPress.vue'),
    StrategyExamples: () => import(/* webpackChunkName: `strategy_examples` */ '../../components/api_blocks/StrategyExamples.vue'),
    StrategyBanner: () => import(/* webpackChunkName: `strategy_banner` */ '../../components/api_blocks/StrategyBanner.vue'),
    StrategyAdvantages: () => import(/* webpackChunkName: `strategy_advantages` */ '../../components/api_blocks/StrategyAdvantages.vue'),
    StrategyDescription: () => import(/* webpackChunkName: `strategy_description` */ '../../components/api_blocks/StrategyDescription.vue'),
    StrategyChart: () => import(/* webpackChunkName: `strategy_chart` */ '../../components/api_blocks/StrategyChart.vue'),
    StrategyConditions: () => import(/* webpackChunkName: `strategy_conditions` */ '../../components/api_blocks/StrategyConditions.vue'),
    ProfessionalTeam: () => import(/* webpackChunkName: `professional_team` */ '../../components/api_blocks/ProfessionalTeam.vue'),
    BrokerTariffs: () => import(/* webpackChunkName: `broker_tariffs` */ '../../components/api_blocks/BrokerTariffs.vue'),
    ClientReviews: () => import(/* webpackChunkName: `client_reviews` */ '../../components/api_blocks/ClientReviews.vue'),
    TradeAdvantages: () => import(/* webpackChunkName: `trade_advantages` */ '../../components/api_blocks/TradeAdvantages.vue'),
    TradeTools: () => import(/* webpackChunkName: `trade_tools` */ '../../components/api_blocks/TradeTools.vue'),
    TradeQuik: () => import(/* webpackChunkName: `trade_quik` */ '../../components/api_blocks/TradeQuik.vue'),
    TabsAnalitics: () => import(/* webpackChunkName: `tabs_analitics` */ '../../components/api_blocks/TabsAnalitics.vue'),
    PressCenters: () => import(/* webpackChunkName: `press_centers` */ '../../components/api_blocks/PressCenters.vue'),
    AboutRatings: () => import(/* webpackChunkName: `about_ratings` */ '../../components/api_blocks/AboutRatings.vue'),
    Gallery: () => import(/* webpackChunkName: `gallery` */ '../../components/api_blocks/Gallery.vue'),
    AuctionServices: () => import(/* webpackChunkName: `auction_services` */ '../../components/api_blocks/AuctionServices.vue'),
    AuctionProcedures: () => import(/* webpackChunkName: `auction_procedures` */ '../../components/api_blocks/AuctionProcedures.vue'),
    MarketsPerson: () => import(/* webpackChunkName: `markets_person` */ '../../components/api_blocks/MarketsPerson.vue'),
    MarketsServices: () => import(/* webpackChunkName: `markets_services` */ '../../components/api_blocks/MarketsServices.vue'),
    MarketsMedia: () => import(/* webpackChunkName: `markets_media` */ '../../components/api_blocks/MarketsMedia.vue'),
    EvaluationServices: () => import(/* webpackChunkName: `evaluation_services` */ '../../components/api_blocks/EvaluationServices.vue'),
    EvaluationLicenses: () => import(/* webpackChunkName: `evaluation_licenses` */ '../../components/api_blocks/EvaluationLicenses.vue'),
    EvaluationSpecialists: () => import(/* webpackChunkName: `evaluation_specialists` */ '../../components/api_blocks/EvaluationSpecialists.vue'),
    EvaluationClienst: () => import(/* webpackChunkName: `evaluation_clienst` */ '../../components/api_blocks/EvaluationClienst.vue'),

    IconAppInstruction: () => import(/* webpackChunkName: `icon_app_instruction` */ '../../components/icons/icon_files/IconAppInstruction.vue'),
    IconEasy: () => import(/* webpackChunkName: `icon_easy` */ '../../components/icons/icon_files/IconEasy.vue'),
    IconProfitable: () => import(/* webpackChunkName: `icon_profitable` */ '../../components/icons/icon_files/IconProfitable.vue'),
    IconHandy: () => import(/* webpackChunkName: `icon_handy` */ '../../components/icons/icon_files/IconHandy.vue'),
    IconManagement: () => import(/* webpackChunkName: `icon_management` */ '../../components/icons/icon_files/IconManagement.vue'),
    IconExchange: () => import(/* webpackChunkName: `icon_exchange` */ '../../components/icons/icon_files/IconExchange.vue'),
    IconTools: () => import(/* webpackChunkName: `icon_tools` */ '../../components/icons/icon_files/IconTools.vue'),
    IconTop5: () => import(/* webpackChunkName: `icon_top5` */ '../../components/icons/icon_files/IconTop5.vue'),
    IconTop20: () => import(/* webpackChunkName: `icon_top20` */ '../../components/icons/icon_files/IconTop20.vue'),
    IconTop25: () => import(/* webpackChunkName: `icon_top25` */ '../../components/icons/icon_files/IconTop25.vue'),
    IconHelp: () => import(/* webpackChunkName: `icon_help` */ '../../components/icons/icon_files/IconHelp.vue'),
    IconExperience: () => import(/* webpackChunkName: `icon_experience` */ '../../components/icons/icon_files/IconExperience.vue'),
    IconOffices: () => import(/* webpackChunkName: `icon_offices` */ '../../components/icons/icon_files/IconOffices.vue'),
    IconCbonds: () => import(/* webpackChunkName: `icon_cbonds` */ '../../components/icons/icon_files/IconCbonds.vue'),
    IconLocation: () => import(/* webpackChunkName: `icon_location` */ '../../components/icons/icon_files/IconLocation.vue'),
    IconEmail: () => import(/* webpackChunkName: `icon_email` */ '../../components/icons/icon_files/IconEmail.vue'),
    IconPhone: () => import(/* webpackChunkName: `icon_phone` */ '../../components/icons/icon_files/IconPhone.vue'),
    IconTime: () => import(/* webpackChunkName: `icon_time` */ '../../components/icons/icon_files/IconTime.vue'),
    IconMoneyBox: () => import(/* webpackChunkName: `icon_moneybox` */ '../../components/icons/icon_files/IconMoneyBox.vue'),
    IconRequirements: () => import(/* webpackChunkName: `icon_requirements` */ '../../components/icons/icon_files/IconRequirements.vue'),
    IconTransparency: () => import(/* webpackChunkName: `icon_transparency` */ '../../components/icons/icon_files/IconTransparency.vue'),
    IconQualityShield: () => import(/* webpackChunkName: `icon_qualityshield` */ '../../components/icons/icon_files/IconQualityShield.vue'),
    IconUniversality: () => import(/* webpackChunkName: `icon_universality` */ '../../components/icons/icon_files/IconUniversality.vue'),
    IconProfessionalism: () => import(/* webpackChunkName: `icon_professionalism` */ '../../components/icons/icon_files/IconProfessionalism.vue'),
    IconReliability: () => import(/* webpackChunkName: `icon_reliability` */ '../../components/icons/icon_files/IconReliability.vue'),
    IconPositive: () => import(/* webpackChunkName: `icon_positive` */ '../../components/icons/icon_files/IconPositive.vue'),
    IconNeutral: () => import(/* webpackChunkName: `icon_neutral` */ '../../components/icons/icon_files/IconNeutral.vue'),
    IconNegative: () => import(/* webpackChunkName: `icon_negative` */ '../../components/icons/icon_files/IconNegative.vue'),
    IconComfortable: () => import(/* webpackChunkName: `icon_comfortable` */ '../../components/icons/icon_files/IconComfortable.vue'),
    IconResults: () => import(/* webpackChunkName: `icon_results` */ '../../components/icons/icon_files/IconResults.vue'),
    IconStrategy: () => import(/* webpackChunkName: `icon_strategy` */ '../../components/icons/icon_files/IconStrategy.vue'),
    IconKnowledge: () => import(/* webpackChunkName: `icon_knowledge` */ '../../components/icons/icon_files/IconKnowledge.vue'),
    IconPenalty: () => import(/* webpackChunkName: `icon_penalty` */ '../../components/icons/icon_files/IconPenalty.vue'),
    IconFacebook: () => import(/* webpackChunkName: `icon_facebook` */ '../../components/icons/icon_files/IconFacebook.vue'),
    IconTelegram: () => import(/* webpackChunkName: `icon_telegram` */ '../../components/icons/icon_files/IconTelegram.vue'),
    IconInstagram: () => import(/* webpackChunkName: `icon_instagram` */ '../../components/icons/icon_files/IconInstagram.vue'),
    IconVkontakte: () => import(/* webpackChunkName: `icon_vkontakte` */ '../../components/icons/icon_files/IconVkontakte.vue'),
    IconYoutube: () => import(/* webpackChunkName: `icon_youtube` */ '../../components/icons/icon_files/IconYoutube.vue'),

    IconLogo,
    IconLogoTextTop,
    IconLogoTextBottom,
    IconMenuToggle,
    IconCall,
    IconChewronDown,
    IconMapPin,
    IconLogIn,
    IconSearch,
    IconAppStore,
    IconGooglePlay,
    IconFilePdf,
    IconChewronLeft,
    IconStrat,
    IconMoney,
    IconCalendar,
    IconIdeas,
    IconQuality,
    IconTelegramMen,
    IconSolidApp,
    IconGuarantee,
    IconRefill,
    IconIncome,
    IconMulticurrency,
    IconFlexible,
    IconDocs,
    IconClose,
    IconDownload,

    ElDropdown,
    ElCustomScroll,
    ElPopupSearch,
    DropdownCities,
    SiteMenu,
    SiteSocials
}
