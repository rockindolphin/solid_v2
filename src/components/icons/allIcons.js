import IconLogo from './icon_files/IconLogo.vue';
import IconLogoTextTop from './icon_files/IconLogoTextTop.vue';
import IconLogoTextBottom from './icon_files/IconLogoTextBottom.vue';
import IconMenuToggle from './icon_files/IconMenuToggle.vue';
import IconCall from './icon_files/IconCall.vue';
import IconChewronDown from './icon_files/IconChewronDown.vue';
import IconMapPin from './icon_files/IconMapPin.vue';
import IconLogIn from './icon_files/IconLogIn.vue';
import IconAppStore from './icon_files/IconAppStore.vue';
import IconGooglePlay from './icon_files/IconGooglePlay.vue';
import IconAppInstruction from './icon_files/IconAppInstruction.vue';
import IconFacebook from './icon_files/IconFacebook.vue';
import IconTelegram from './icon_files/IconTelegram.vue';
import IconInstagram from './icon_files/IconInstagram.vue';
import IconVkontakte from './icon_files/IconVkontakte.vue';
import IconYoutube from './icon_files/IconYoutube.vue';
import IconFilePdf from './icon_files/IconFilePdf.vue';
import IconChewronLeft from './icon_files/IconChewronLeft.vue';
import IconStrat from './icon_files/IconStrat.vue';
import IconMoney from './icon_files/IconMoney.vue';
import IconCalendar from './icon_files/IconCalendar.vue';
import IconIdeas from './icon_files/IconIdeas.vue';
import IconQuality from './icon_files/IconQuality.vue';
import IconTelegramMen from './icon_files/IconTelegramMen.vue';
import IconMoex from './icon_files/IconMoex.vue';
import IconMoexCurrency from './icon_files/IconMoexCurrency.vue';
import IconMoexUrgent from './icon_files/IconMoexUrgent.vue';
import IconFinWorld from './icon_files/IconFinWorld.vue';
import IconFinSpb from './icon_files/IconFinSpb.vue';
import IconSolidApp from './icon_files/IconSolidApp.vue';
import IconApple from './icon_files/IconApple.vue';
import IconGplay from './icon_files/IconGplay.vue';
import IconGuarantee from './icon_files/IconGuarantee.vue';
import IconRefill from './icon_files/IconRefill.vue';
import IconIncome from './icon_files/IconIncome.vue';
import IconMulticurrency from './icon_files/IconMulticurrency.vue';
import IconFlexible from './icon_files/IconFlexible.vue';
import IconDocs from './icon_files/IconDocs.vue';
import IconClose from './icon_files/IconClose.vue';
import IconProfitable from './icon_files/IconProfitable.vue';
import IconEasy from './icon_files/IconEasy.vue';
import IconHandy from './icon_files/IconHandy.vue';
import IconManagement from './icon_files/IconManagement.vue';
import IconExchange from './icon_files/IconExchange.vue';
import IconTools from './icon_files/IconTools.vue';
import IconTop5 from './icon_files/IconTop5.vue';
import IconTop20 from './icon_files/IconTop20.vue';
import IconTop25 from './icon_files/IconTop25.vue';
import IconHelp from './icon_files/IconHelp.vue';
import IconExperience from './icon_files/IconExperience.vue';
import IconOffices from './icon_files/IconOffices.vue';
import IconCbonds from './icon_files/IconCbonds.vue';
import IconDownload from './icon_files/IconDownload.vue';
import IconLocation from './icon_files/IconLocation.vue';
import IconEmail from './icon_files/IconEmail.vue';
import IconPhone from './icon_files/IconPhone.vue';
import IconTime from './icon_files/IconTime.vue';
import IconMoneyBox from './icon_files/IconMoneyBox.vue';
import IconRequirements from './icon_files/IconRequirements.vue';
import IconTransparency from './icon_files/IconTransparency.vue';
import IconQualityShield from './icon_files/IconQualityShield.vue';
import IconUniversality from './icon_files/IconUniversality.vue';
import IconProfessionalism from './icon_files/IconProfessionalism.vue';
import IconReliability from './icon_files/IconReliability.vue';
import IconPositive from './icon_files/IconPositive.vue';
import IconNeutral from './icon_files/IconNeutral.vue';
import IconNegative from './icon_files/IconNegative.vue';
import IconSearch from './icon_files/IconSearch.vue';
import IconComfortable from './icon_files/IconComfortable.vue';
import IconResults from './icon_files/IconResults.vue';
import IconStrategy from './icon_files/IconStrategy.vue';
import IconKnowledge from './icon_files/IconKnowledge.vue';
import IconPenalty from './icon_files/IconPenalty.vue';
import IconIndependence from './icon_files/IconIndependence.vue';
import IconBorrowing from './icon_files/IconBorrowing.vue';
import IconStructure from './icon_files/IconStructure.vue';

export default {
	IconGazprom: () => import(/* webpackChunkName: `icon_gazprom` */ './icon_files/IconGazprom.vue'),
	IconSearch,
	IconTelegramMen,
	IconLogo,
	IconLogoTextTop,
	IconLogoTextBottom,
	IconMenuToggle,
	IconCall,
	IconChewronDown,
	IconMapPin,
	IconLogIn,
	IconAppStore,
	IconGooglePlay,
	IconAppInstruction,
	IconFacebook,
	IconTelegram,
	IconInstagram,
	IconVkontakte,
	IconYoutube,
	IconFilePdf,
	IconChewronLeft,
	IconStrat,
	IconMoney,
	IconCalendar,
	IconIdeas,
	IconQuality,
	IconMoex,
	IconMoexCurrency,
	IconMoexUrgent,
	IconFinWorld,
	IconFinSpb,
	IconSolidApp,
	IconApple,
	IconGplay,
	IconGuarantee,
	IconRefill,
	IconIncome,
	IconMulticurrency,
	IconFlexible,
	IconDocs,
	IconClose,
	IconProfitable,
	IconEasy,
	IconHandy,
	IconManagement,
	IconExchange,
	IconTools,
	IconTop5,
	IconTop20,
	IconTop25,
	IconHelp,
	IconExperience,
	IconOffices,
	IconCbonds,
	IconDownload,
	IconLocation,
	IconEmail,
	IconPhone,
	IconTime,
	IconMoneyBox,
	IconRequirements,
	IconTransparency,
	IconQualityShield,
	IconUniversality,
	IconProfessionalism,
	IconReliability,
	IconPositive,
	IconNeutral,
	IconNegative,
	IconComfortable,
	IconResults,
	IconStrategy,
	IconKnowledge,
	IconPenalty,
	IconIndependence,
	IconBorrowing,
	IconStructure
}