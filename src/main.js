import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store';
import YmapPlugin from 'vue-yandex-maps';
import VueScrollmagic from 'vue-scrollmagic';
import './styles/style.scss';
import './styles/style.css';

// for ie11
import 'core-js/es/number';
import { customEventPolyfill } from './libs/customEventPolyfill.js';
customEventPolyfill();

Vue.config.productionTip = false

Vue.prototype.$evbus = new Vue(); // Global event bus

Vue.use(YmapPlugin);

Vue.use(VueScrollmagic);

Vue.directive('backgroundImage', {
	inserted: function (el) {

		function getImageSrc(img){
			let scrProp = window.HTMLPictureElement ? 'currentSrc' : 'src';
			return new Promise( (resolve, reject) => {
				if (img.complete) {
					resolve( img[scrProp] );
				}else{
					img.addEventListener('load', function(){
						resolve(img[scrProp]);
					},{ once: true });
					img.addEventListener('error', function(){
						reject('fallback_img_url');
					},{ once: true });
				}
			});
		}

		if ( el.nodeName.toUpperCase() === 'PICTURE' ){
			getImageSrc( el.querySelector('img') ).then( src => {
				el.parentNode.style.backgroundImage = `url(${src})`;
			});

			if ( window.HTMLPictureElement ){
				Object.keys(store.state.breakpoints).map( breakpoint_key => {
					window.matchMedia(`(min-width:${store.state.breakpoints[breakpoint_key]}px)`).addListener( () => {
						getImageSrc( el.querySelector('img') ).then(src => {
							el.parentNode.style.backgroundImage = `url(${src})`;
						});
					});
				});
			}
		}else{
			getImageSrc( el ).then(src => {
				el.parentNode.style.backgroundImage = `url(${src})`;
			});
		}
		el.style.display = 'none';

	},
});

Vue.directive('rangeProgress', {
	inserted: function (el) {
		let min = el.min || 0,
			max = el.max || 100;
		el.style.setProperty('--range-progress', `${ (el.value*100/(max-min) - min*100/max).toFixed(0) }%`);
		el.addEventListener('input', () => {
			el.style.setProperty('--range-progress', `${ (el.value*100/(max-min) - min*100/max).toFixed(0) }%`);
		}, false);
	},
});

Vue.filter('money', function (value, symbol = 'руб.') {
	return `${new Intl.NumberFormat('ru-RU', {
		minimumFractionDigits: 0,
		maximumFractionDigits: 2
	}).format(value)} ${symbol}`;
});

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app')
