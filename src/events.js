export const events = {
    EV_APP_TOGGLE_PAGE_SCROLL: 'toggle-page-scroll',
    EV_APP_OPEN_POPUP: 'open-popup',
    EV_APP_CLOSE_POPUP: 'close-popup',
    EV_APP_CLOSE_ALL_WINDOWS: 'close-all-windows',
    EV_APP_CHANGE_CITY: 'change-city',
    EV_APP_TOGGLE_TAB: 'toggle-tab',
}
