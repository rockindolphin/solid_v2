import Vue from 'vue'
import Router from 'vue-router'
import PageIndex from './views/PageIndex';
import PageAnalitics from './views/PageAnalitics';
import PageCompanyAnalitics from './views/PageCompanyAnalitics';
import PagePersonalBroker from './views/PagePersonalBroker';
import PageTrade from './views/PageTrade';
import PageIdu from './views/PageIdu';
import PageIduInner from './views/PageIduInner';
import PageIis from './views/PageIis';
import PageApplication from './views/PageApplication';
import PageAbout from './views/PageAbout';
import PageDocs from './views/PageDocs';
import PageContacts from './views/PageContacts';
import PageTariffs from './views/PageTariffs';
import PageEvaluation from './views/PageEvaluation';
import PageMarkets from './views/PageMarkets';
import PageAuction from './views/PageAuction';
import PageSingleReview from './views/PageSingleReview';
import PagePressCenter from './views/PagePressCenter';
import PageSinglePress from './views/PageSinglePress';
import PageAuctionLot from './views/PageAuctionLot';
import PageDcm from './views/PageDcm';

import PageUi from './views/PageUi';

Vue.use(Router);

const router = new Router({
	mode: process.env.VUE_APP_ROUTER_MODE,
	base: process.env.VUE_APP_ROUTER_BASE,
	linkActiveClass: "active-path",
	linkExactActiveClass: "active",
	routes: [
		{
			path: '/',
			name: 'index',
			component: PageIndex,
			meta: {
				breadCrumb: 'Главная',
				parent: null
			}
		},
		{
			path: '/analitics',
			name: 'analitics',
			component: PageAnalitics,
			meta: {
				breadCrumb: 'Аналитика',
				parent: 'index'
			}
		},
		{
			path: '/company-analitics',
			name: 'company-analitics',
			component: PageCompanyAnalitics,
			meta: {
				breadCrumb: 'Дивидендная доходность акций Эталон Групп превышает 10%',
				parent: 'analitics'
			}
		},
		{
			path: '/personal-broker',
			name: 'personal-broker',
			component: PagePersonalBroker,
			meta: {
				breadCrumb: 'Персональный брокер',
				parent: 'index'
			}
		},
		{
			path: '/trade',
			name: 'trade',
			component: PageTrade,
			meta: {
				breadCrumb: 'Купить акции',
				parent: 'index'
			}
		},
		{
			path: '/idu',
			name: 'idu',
			component: PageIdu,
			meta: {
				breadCrumb: 'Доверительное управление инвестициями',
				parent: 'index'
			}
		},
		{
			path: '/idu-inner',
			name: 'idu-inner',
			component: PageIduInner,
			meta: {
				breadCrumb: 'Стратегия Российские акции 1',
				parent: 'idu'
			}
		},
		{
			path: '/iis',
			name: 'iis',
			component: PageIis,
			meta: {
				breadCrumb: 'ИИС',
				parent: 'index'
			}
		},
		{
			path: '/application',
			name: 'application',
			component: PageApplication,
			meta: {
				breadCrumb: 'Мобильное приложение',
				parent: 'index'
			}
		},
		{
			path: '/about',
			name: 'about',
			component: PageAbout,
			meta: {
				breadCrumb: 'О компании',
				parent: 'index'
			}
		},
		{
			path: '/docs',
			name: 'docs',
			component: PageDocs,
			meta: {
				breadCrumb: 'Документы',
				parent: 'about'
			}
		},
		{
			path: '/press-center',
			name: 'press-center',
			component: PagePressCenter,
			meta: {
				breadCrumb: 'Пресс-центр',
				parent: 'about'
			}
		},
		{
			path: '/single-press',
			name: 'single-press',
			component: PageSinglePress,
			meta: {
				breadCrumb: 'ИФК «Солид» — победитель в 4 номинациях премии Cbonds Awards-2020',
				parent: 'press-center'
			}
		},
		{
			path: '/contacts',
			name: 'contacts',
			component: PageContacts,
			meta: {
				breadCrumb: 'Контакты',
				parent: 'about'
			}
		},
		{
			path: '/tariffs',
			name: 'tariffs',
			component: PageTariffs,
			meta: {
				breadCrumb: 'Тарифы',
				parent: 'index'
			}
		},
		{
			path: '/evaluation',
			name: 'evaluation',
			component: PageEvaluation,
			meta: {
				breadCrumb: 'Оценка',
				parent: 'index'
			}
		},
		{
			path: '/markets',
			name: 'markets',
			component: PageMarkets,
			meta: {
				breadCrumb: 'Рынки капитала',
				parent: 'index'
			}
		},
		{
			path: '/auction',
			name: 'auction',
			component: PageAuction,
			meta: {
				breadCrumb: 'Аукционный центр',
				parent: 'index'
			}
		},
		{
			path: '/single-review',
			name: 'single-review',
			component: PageSingleReview,
			meta: {
				breadCrumb: 'Еженедельный обзор инвестидей 7-11 декабря 2020',
				parent: 'index'
			}
		},
		{
			path: '/auction-lot',
			name: 'auction-lot',
			component: PageAuctionLot,
			meta: {
				breadCrumb: 'Прямая продажа',
				parent: 'auction'
			}
		},
		{
			path: '/dcm',
			name: 'dcm',
			component: PageDcm,
			meta: {
				breadCrumb: 'Рынки капитала',
				parent: 'iis'
			}
		},
		{
			path: '/ui',
			name: 'ui',
			component: PageUi
		}
	]
});


export default router;
