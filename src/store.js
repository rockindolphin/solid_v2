import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
	strict: debug,
	plugins: debug ? [createLogger()] : [],

	state: {
		project: {
			title: 'Солид Брокер',
			description: 'Рынки капиталов',
			copy: 'АО ИФК «Солид». Все права защищены. Информация и мнения, представленные на данном ресурсе, подготовлены специалистами компании ИФК «Солид». Полное или частичное предоставление материалов третьим лицам возможно в случаях и на условиях, определенных законодательством. Настоящий документ не может рассматриваться в качестве публичной оферты. ИФК «Солид», его руководство и сотрудники не несут ответственности за инвестиционное решение клиента, основанное на информации, содержащейся в настоящем буклете. Лицензии на осуществление: - брокерской деятельности – № 045-06790-100000, выдана ФКЦБ России 24 июня 2003 г. без ограничения срока действия; - дилерской деятельности – № 045-06793-010000, выдана ФКЦБ России 24 июня 2003 г. без ограничения срока действия; - деятельности по управлению ценными бумагами – № 045-06795-001000, выдана ФКЦБ России 24 июня 2003 г. без ограничения срока действия; - депозитарной деятельности – № 045-06807-000100, выдана ФКЦБ России 27 июня 2003 г. без ограничения срока действия \r\nИндивидуальные инвестиционные рекомендации могут предоставляться исключительно в рамках заключенного Договора об инвестиционном консультировании.Рекомендации, предоставляемые в рамках сервисов сайта не являются индивидуальными инвестиционными рекомендациями.'
		},
		phone: '8 (800) 250-70-10',
		phone_tech: '8 (495) 228-70-14',
		phone_traiding: '8 (495) 228-70-16',
		appStoreUrl: 'https://apps.apple.com/ru/app/%D1%81%D0%BE%D0%BB%D0%B8%D0%B4-%D0%B8%D0%BD%D0%B2%D0%B5%D1%81%D1%82%D0%BE%D1%80/id1495333071?mt=8',
		googlePlayUrl: 'https://play.google.com/store/apps/details?id=ru.solidbroker.investor&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1',
		AppInstruction: '#',
		facebookUrl: 'https://www.facebook.com/solidbroker',
		telegramUrl: 'https://tele.click/SolidPro',
		instagramUrl: 'https://www.instagram.com/solid.broker/',
		vkontakteUrl: 'https://vk.com/solidbroker',
		youtubeUrl: 'https://www.youtube.com/user/solidifc/featured',
		isEdge: /Edge/.test(navigator.userAgent),
		isIE: /Trident/.test(navigator.userAgent),
		isSafari: navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1,
		breakpoints: JSON.parse(process.env.VUE_APP_BREAKPOINTS),
		menu_main: [
			{
				title: 'Доверительное управление',
				route_name: 'idu'
			},
			{
				title: 'Персональный брокер',
				route_name: 'personal-broker'
			},
			{
				title: 'Купить акции',
				route_name: 'trade'
			},
			{
				title: 'ИИС',
				route_name: 'iis'
			},
			{
				title: 'Аналитика',
				route_name: 'analitics'
			},
			{
				title: 'О компании',
				route_name: 'about',
				inner: [
					{
						title: 'Контакты',
						route_name: 'contacts',
					},
					{
						title: 'Документы',
						route_name: 'docs'
					},
					{
						title: 'Пресс-центр',
						route_name: 'press-center'
					}
				]
			}
		],
		menu_secondary: [
			{
				title: 'Мобильное приложение',
				route_name: 'application'
			},
			{
				title: 'Инструкции'
			},
			{
				title: 'Рынки капитала',
				route_name: 'markets'
			},
			{
				title: 'Тарифы на брокерское обслуживание',
				route_name: 'tariffs'
			},
			{
				title: 'Аукционный центр',
				route_name: 'auction'
			},
			{
				title: 'Оценка',
				route_name: 'evaluation'
			},
			{
				title: 'Раскрытие информации'
			}
		]
	},
	getters: {
		copy(state) {
			return `© ${new Date().getFullYear()} ${state.project.copy}`
		},
		animEnabled(state) {
			return !(state.isEdge || state.isIE);
		}
	},
	mutations: {

	},
	actions: {

	}
});
