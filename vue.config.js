let postcss = require('postcss'),
    postcssImport = require('postcss-import'),
    postcssMixins = require('postcss-mixins'),
    postcssPresetEnv = require('postcss-preset-env'),
    cssBreakpoints = JSON.parse(process.env.VUE_APP_BREAKPOINTS),
    postcssPresetEnvMedia = {},
    postcssPresetEnvProperties = {},
    sassData = '';

Object.keys(cssBreakpoints).map(key => {
    postcssPresetEnvMedia[`--${key}-viewport`] = `(min-width: ${cssBreakpoints[key]}px)`;
    postcssPresetEnvProperties[`--${key}`] = `${cssBreakpoints[key]}px`;
    sassData += `$${key}-breakpoint: ${cssBreakpoints[key]}px;`;
})

module.exports = {
    publicPath: process.env.NODE_ENV === 'development' ? '/' : '',
    css: {
        loaderOptions: {
            css: {
                // эти настройки будут переданы в css-loader
            },
            sass: {
                additionalData: sassData
            },
            postcss: {
                ident: 'postcss',
                plugins: () => [
                    postcssImport({

                    }),
                    postcssMixins({
                        mixins: {
                            addJsVariablesToRoot(mixin){
                                var rule = postcss.rule({ selector: ':root' });
                                Object.keys(cssBreakpoints).map(key => {
                                    rule.append({
                                        prop: `--${key}`,
                                        value: `${cssBreakpoints[key]}px`
                                    });
                                });
                                mixin.replaceWith(rule);
                            },
                            edgeOnly(mixin, prop, value){
                                let rule = `
                                    @supports(-ms-ime-align: auto) {
                                        ${prop}: ${value};
                                    }
                                `;
                                mixin.replaceWith(rule);
                            },
                            backgroundImg(mixin, prop, value){
                                let rule = `
                                    display: block;
                                    position: absolute;
                                    top: 50%;
                                    left: 50%;
                                    transform: translateX(-50%) translateY(-50%);
                                    min-width: 100%;
                                    min-height: 100%;
                                `;
                                mixin.replaceWith(rule);
                            },
                            cssLock(mixin, prop, breakpoint1, breakpoint2, y1, y2){
                                let x1 = cssBreakpoints[breakpoint1],
                                    x2 = cssBreakpoints[breakpoint2],
                                    m = (y2 - y1) / (x2 - x1),
                                    b = y1 - m * x1,
                                    rule = '';

                                //toFixed(10) - ie11 тупит при >10 знаков после запятой
                                rule = `${prop}: calc( ${m.toFixed(10)}*100vw ${b > 0 ? '+ ' : '- '} ${Math.abs(b.toFixed(10))}px);`;
                                mixin.replaceWith(rule);
                            }
                        }
                    }),
                    postcssPresetEnv({
                        stage: 0,
                        importFrom: [
                            {
                                customProperties: postcssPresetEnvProperties,
                                customMedia: postcssPresetEnvMedia
                            }
                        ],
                        features: {
                            customProperties: {
                                preserve: true
                            }
                        }
                    })
                ]
            }
        }
    }
}