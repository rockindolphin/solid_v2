const { src, dest, series, parallel, watch, task, lastRun } = require('gulp'),
    fs = require('fs'),
    pug = require('gulp-pug'),
    path = require('path'),
    del = require('del'),
    gulpif = require('gulp-if'),
    args = require('yargs').argv,
    server = require('gulp-server-livereload'),
    miss = require('mississippi'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    gulpPostcss = require('gulp-postcss'),
    postcss = require('postcss'),
    postcssImport = require("postcss-import"),
    postcssPresetEnv = require('postcss-preset-env')
    autoprefixer = require('gulp-autoprefixer'),
    postcssMixins = require('postcss-mixins'),
    csso = require('gulp-csso'),
    named = require('vinyl-named'),
    VueLoaderPlugin = require('vue-loader/lib/plugin'),
    webpack = require('webpack'),
    webpackStream = require('webpack-stream'),
    insert = require('gulp-insert'),
    notifier = require('node-notifier');

require('dotenv').config({
    path: (args.prod || args.production) ? '.env' : '.env.development'
});

const config = {
    src: {
        html: 'src/pug/*.pug',
        images: 'src/assets/images/**/*',
        fonts: 'src/assets/fonts/*.*',
        css: {
            postcss: 'src/styles/style.css',
            scss: 'src/styles/style.scss'
        },
        manifest: 'public/icons/*.*',
        js: 'src/js/app.js'
    },
    dest: {
        html: 'dist',
        images: 'dist/assets/img',
        fonts: 'dist/assets/fonts',
        css: 'dist/css',
        manifest: 'dist/icons',
        js: './dist/js'
    },
    watch: {
        html: 'src/pug/*.pug',
        images: 'src/assets/images/**/*',
        fonts: 'src/assets/fonts/*.*',
        css: [
            'src/styles/**/*.css',
            'src/styles/**/*.scss'
        ],
        manifest: 'public/icons//*.*',
        js: 'src/js/**/*.js'
    },
    cssBlocksFile: 'src/css/blocks/items.json',
}

let cssBreakpoints = JSON.parse(process.env.VUE_APP_BREAKPOINTS),
    postcssPresetEnvMedia = {},
    postcssPresetEnvProperties = {},
    sassData = '';

Object.keys(cssBreakpoints).map(key => {
    postcssPresetEnvMedia[`--${key}-viewport`] = `(min-width: ${cssBreakpoints[key]}px)`;
    postcssPresetEnvProperties[`--${key}`] = `${cssBreakpoints[key]}px`;
    sassData += `$${key}-breakpoint: ${cssBreakpoints[key]}px;`;
});

function err_log(error) {
    console.log([
        '',
        "----------ERROR MESSAGE START----------",
        ("[" + error.name + " in " + error.plugin + "]"),
        error.message,
        "----------ERROR MESSAGE END----------",
        ''
    ].join('\n'));
    notifier.notify({ title: 'Error', message: error.plugin });
}

function addJsVariablesToRoot(mixin) {
    var rule = postcss.rule({ selector: ':root' });
    Object.keys(cssBreakpoints).map(key => {
        rule.append({
            prop: `--${key}`,
            value: `${cssBreakpoints[key]}px`
        });
    });
    mixin.replaceWith(rule);
}

function edgeOnly(mixin, prop, value) {
    let rule = `
        @supports(-ms-ime-align: auto) {
            ${prop}: ${value};
        }
    `;
    mixin.replaceWith(rule);
}

function backgroundImg(mixin, prop, value) {
    let rule = `
        display: block;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translateX(-50%) translateY(-50%);
        min-width: 100%;
        min-height: 100%;
    `;
    mixin.replaceWith(rule);
}

function cssLock(mixin, prop, breakpoint1, breakpoint2, y1, y2) {
    let x1 = cssBreakpoints[breakpoint1],
        x2 = cssBreakpoints[breakpoint2],
        m = (y2 - y1) / (x2 - x1),
        b = y1 - m * x1,
        rule = '';

    //toFixed(10) - ie11 тупит при >10 знаков после запятой
    rule = `${prop}: calc( ${m.toFixed(10)}*100vw ${b > 0 ? '+ ' : '- '} ${Math.abs(b.toFixed(10))}px);`;
    mixin.replaceWith(rule);
}

function html(opts) {
    return miss.pipe(
        src(opts.src, { since: opts.since }),
        pug({
            pretty: args.prod ? false : '\t',
            doctype: 'html',
            locals: {
                config: config,
                path: path,
            }
        }),
        dest(opts.dest),
        (err) => {
            if (err) return err_log(err);
        }
    )
}

function css(opts) {
    return miss.pipe(
        src(opts.src.scss, { sourcemaps: true }),
        insert.prepend(sassData),
        sass(),
        src(opts.src.postcss, { sourcemaps: true }),
        gulpPostcss([
            postcssImport({ path: 'src/styles' }),
            postcssMixins({
                mixins: {
                    addJsVariablesToRoot: addJsVariablesToRoot,
                    edgeOnly: edgeOnly,
                    backgroundImg: backgroundImg,
                    cssLock: cssLock
                }
            }),
            postcssPresetEnv({
                stage: 0,
                importFrom: [
                    {
                        customProperties: postcssPresetEnvProperties,
                        customMedia: postcssPresetEnvMedia
                    }
                ],
                features: {
                    customProperties: {
                        preserve: true
                    }
                }
            })
        ]),
        concat('style.css'),
        autoprefixer(),
        gulpif(
            (args.prod || args.production) || args.cleanCSS,
            csso({
                restructure: false
            })
        ),
        dest(opts.dest, { sourcemaps: '.' }),
        (err) => {
            if (err) return err_log(err);
        }
    )
}

function copy(opts) {
    return miss.pipe(
        src(opts.src, { since: opts.since }),
        dest(opts.dest),
        (err) => {
            if (err) return err_log(err);
        }
    )
}

function js(opts) {
    return miss.pipe(
        src(opts.src, { since: opts.since, sourcemaps: true }),
        named(),
        webpackStream({
            mode: `${(args.prod || args.production) ? 'production' : 'development'}`,
            module: {
                rules: [
                    {
                        test: /\.vue$/,
                        loader: 'vue-loader'
                    },
                    {
                        test: /\.(js)$/,
                        exclude: /(node_modules)/,
                        loader: 'babel-loader'
                    },
                    {
                        test: /\.css$/,
                        use: [
                            'vue-style-loader',
                            'css-loader'
                        ]
                    },
                    {
                        test: /\.pug$/,
                        loader: 'pug-plain-loader'
                    },
                    {
                        test: /\.(png|jpe?g|gif)$/i,
                        use: [
                            {
                                loader: 'file-loader',
                                options: {
                                    name: '[name].[ext]',
                                    outputPath: '../assets/img'
                                }
                            }
                        ]
                    }
                ]
            },
            optimization: {
                splitChunks: {
                    automaticNameDelimiter: '_',
                }
            },
            output: {
                filename: '[name].js',
                chunkFilename: 'chunk_[name]_app.js'
            },
            resolve: {
                //default: mainFields: ['browser', 'module', 'main'] - ie11 fail
                mainFields: ['browser', 'main']
            },
            plugins: [
                new VueLoaderPlugin(),
                new webpack.DefinePlugin({
                    'process.env': JSON.stringify(process.env),
                })
            ]
        }),
        dest(opts.dest, { sourcemaps: '.' }),
        (err) => {
            if (err) return err_log(err);
        }
    )
}

function htmlTask(opts, a, b) {
    return html({
        src: opts.src || config.src.html,
        dest: opts.dest || config.dest.html,
        since: opts.since || lastRun(htmlTask)
    });
}

function jsTask(opts) {
    return js({
        src: opts.src || config.src.js,
        dest: opts.dest || config.dest.js,
        since: opts.since || lastRun(jsTask)
    });
}

function imagesTask(opts) {
    return copy({
        src: opts.src || config.src.images,
        dest: opts.dest || config.dest.images,
        since: opts.since || lastRun(imagesTask)
    });
}

function fontsTask(opts) {
    return copy({
        src: opts.src || config.src.fonts,
        dest: opts.dest || config.dest.fonts,
        since: opts.since || lastRun(fontsTask)
    });
}

function manifestTask(opts) {
    return copy({
        src: opts.src || config.src.manifest,
        dest: opts.dest || config.dest.manifest,
        since: opts.since || lastRun(manifestTask)
    });
}

function cssTask(opts) {
    return css({
        src: opts.src || config.src.css,
        dest: opts.dest || config.dest.css
    });
}

function cleanTask() {
    return del(['dist/**/*']);
}

function serverTask(cb) {
    return src('dist')
        .pipe(
            server({
                livereload: true,
                port: 9090,
                defaultFile: 'index.html',
                open: false,
                directoryListing: false,
            })
        );
}

function watchTask(cb) {
    //run for all
    watch(config.watch.css, cssTask);
    watch(config.watch.html, htmlTask);
    watch(config.watch.fonts, fontsTask);
    watch(config.watch.images, imagesTask);
    watch(config.watch.manifest, manifestTask);
    watch(config.watch.js, jsTask);

    //run for changed/added files
    let tasks = ['html', 'fonts', 'images', 'manifest', 'js'];
    tasks.map(task => {
        let watcher = watch(config.watch[task]);
        watcher.on('add', function (filePath) {
            let now = new Date();
            //change modification time to trigger task with {since} src param
            fs.utimes(filePath, now, now, cb);
        });
    });
}

const buildTask = series(
    cleanTask,
    parallel(
        htmlTask,
        imagesTask,
        cssTask,
        fontsTask,
        manifestTask,
        jsTask
    )
);

exports.default = series(buildTask, serverTask, watchTask);
exports.build = buildTask;
exports.watch = watchTask;
exports.server = serverTask;
